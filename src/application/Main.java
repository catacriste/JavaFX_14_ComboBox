package application;
	
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
/*ComboBOX � um dropmenudown j� com listeners incorporados
 * � uma collection semelhante a choice box que cont�m as op��es a escolher
 * Listener executa a a��o, dispensando o bot�o
 * 
 * Passo1 - criar a comboBox collection, de Strings
 * Passo2 - adicionar-lhe v�rias string com as op��es pretendidas
 * Passo3 - criar um listener para tratar da escolha
 * 
 * Copiar a classe Main do exercicio anterior
 * Nota : Vamos precisar da alertBox de Utils*/

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
		
			
			ComboBox<String> comboBox = new ComboBox<>();
			
			comboBox.getItems().add("Op��o 1");
			comboBox.getItems().add("Op��o 2");
			comboBox.getItems().add("Op��o 3");
			comboBox.getItems().addAll("Op��o 4", "Op��o 5");
			
			comboBox.setPromptText("Escolha uma op��o");
			
			//Listener - capta uma altera��o na sele��o
			comboBox.setOnAction(e ->{
				
				Utils.alertBox("Sele��o", "Op��o Selecionada\n" + comboBox.getValue());
			});
			
			/*- getSelectionModel - define o m�todo de sele��o dos itens, h� varios :
			 * 		- selectNext. selectPrevious, selectedIndexProprety, etc
			 * 		- selectedItemProprety - permite apenas uma sele��o por objeto
			 * - addListener ativa o detetor de eventos para altera��es no itema
			 * 	-express�o LAMBDA com 3 parametros entrada, dados por selectedItemProperty(v, oldValue , newValue)
			 * 			-v � a lista de op��es
			 * 			-oldValue � item que estava selecionado
			 * 			- newValue � o novo item selecionado*/
			
			
			
			VBox layoutComboBox = new VBox(10);
			layoutComboBox.setPadding(new Insets(20,20,20,20));
			layoutComboBox.getChildren().addAll(comboBox);
			
			
			
			BorderPane root = new BorderPane();
			Scene scene = new Scene(layoutComboBox,300,200);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}